# imports
import cv2
import io
import picamera
import numpy as np
import time


# create in-memory stream
stream = io.BytesIO()
with picamera.PiCamera() as camera:
    camera.start_preview()
    time.sleep(2)
    camera.capture(stream, format='jpeg')

    # Detect object in video stream using Haarcascade Frontal Face
    face_detector = cv2.CascadeClassifier(
        'haarcascade_frontalface_default.xml')
    # For each person, one face id
    face_id = 1
    # Initialize sample face image
    count = 0
    # construct a numpy array from stream
    data = np.fromstring(stream.getvalue(), dtype=np.uint8)

    # start looping
    while(True):
        # decode the image from the array, preserving color
        image = cv2.imdecode(data, 1)
        # Convert frame to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Detect frames of different sizes, list of faces rectangles
        faces = face_detector.detectMultiScale(gray, 1.3, 5)

        # Loops for each faces
        for (x, y, w, h) in faces:

            # Crop the image frame into rectangle
            cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)

            # Increment sample face image
            count += 1

            # Save the captured image into the datasets folder
            cv2.imwrite("dataset/User." + str(face_id) + '.' + str(count) +
                        ".jpg", gray[y:y+h, x:x+w])

            # Display the video, with bounded rectangle on the person's face
            cv2.imshow('frame', image)

            # To stop taking video, press 'q' for at least 100ms
            if cv2.waitKey(100) & 0xFF == ord('q'):
                break

            # If image taken reach 100, stop taking video
            elif count > 100:
                break

# stop video capture
camera.close()
